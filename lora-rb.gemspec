require File.join(File.dirname(__FILE__), "lib", "version")

Gem::Specification.new do |s|
  s.name          = 'lora-rb'
  s.version       = LoraRb.version
  s.date          = LoraRb.updated_at
  s.summary       = 'Manage your devices inside a Lora Network'
  s.description   = 'This gem let you to connect to a Lora Network and manage your registered devices.'
  s.homepage      = 'https://github.com/marcomd/lora-rb'
  s.authors       = ['Marco Mastrodonato', 'Rudi Pettazzi', 'Stefano Piras', 'Andrea Longhi']
  s.required_ruby_version = '>= 2.3.0'
  s.email         = %w(marco.mastrodonato@fractalgarden.com rudi.petazzi@fractalgarden.com stefano.piras@fractalgarden.com)
  s.licenses      = %w(LGPL-3.0)
  s.requirements  = 'The brave to dare'

  s.require_paths = ['lib']
  s.files         = Dir["lib/**/*"] + %w(LICENSE README.md CHANGELOG.md)

  s.add_dependency 'json', '~> 2.1'
  # s.add_dependency 'openssl', '~> 2.0'
  # s.add_dependency 'http', '~> 2.2'
  # s.add_dependency 'bunny', '~> 2.7'
  s.add_dependency 'mqtt', '~> 0.5', '>= 0.5.0'
  s.add_dependency 'philter', '~> 1.2', '>= 1.2.0'
  s.add_development_dependency 'test-unit', '~> 3.0'
end
