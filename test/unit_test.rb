require "test/unit"
require "./lib/lora-rb"
require_relative "helper/base_test"

class UnitTest < Test::Unit::TestCase

  #################
  ### YOUR TESTS
  #################

  # Loriot provider
  # test 'Connect with a device with tls' do
  #   eui = LoraRb::Settings.test_eui
  #   lora = LoraClient.new
  #   assert lora.is_a?(LoraClient), "the instance should be a LoraClient class, not #{lora.class}"
  #
  #   response = lora.send_cmd(eui: eui, confirmed: true)
  #   assert response.is_a?(Hash), "la risposta lora dovrebbe essere un hash"
  #   # assert response['success'] == 'Data enqueued', "should be exists a key 'success' in #{response}"
  #   assert response['cmd'] == 'txd', "response should be cmd=tx, not #{response}"
  #   assert response['EUI'] == eui, "should be the same EUI #{eui} in #{response}"
  #
  #   response = lora.read_data
  #   assert response.is_a?(Hash), "la risposta lora dovrebbe essere un hash"
  #   assert response['EUI'] == eui, "should be the same EUI #{eui} in #{response}"
  #
  #   lora.send_cmd(eui: eui, confirmed: true)
  #   response = lora.listen(test: true)
  #   assert response.is_a?(Hash), "la risposta lora dovrebbe essere un hash"
  #   lora.send_cmd(eui: eui, confirmed: true)
  #   lora.listen(test: true) do |output|
  #     assert output.is_a?(Hash), "la risposta lora di ogni iterazione listen dovrebbe essere un hash"
  #   end
  #   assert response.is_a?(Hash), "la risposta lora dovrebbe essere un hash"
  #
  #   lora.quit
  #   assert lora.tcp_socket.closed?, "after quit, tcp_socket should be closed"
  # end

  # Resiot provider
  # test 'Connect with a device with http' do
  #   eui = LoraRb::Settings.test_eui
  #   lora = LoraClient.new
  #   assert lora.is_a?(LoraClient), "the instance should be a LoraClient class, not #{lora.class}"
  #
  #   response = lora.send_cmd(eui: eui, confirmed: false)
  #   assert response.is_a? Hash
  #   assert response['status_code'] == 200
  #   assert response['Content-Type'] == 'application/json'
  # end

  # A2a provider
  test 'Connect to the lora network with mqtt' do
    lora = LoraClient.new
    assert lora.is_a?(LoraClient), "the instance should be a LoraClient class, not #{lora.class}"
    assert lora.client.is_a?(MQTT::Client)
    assert lora.client.client_id.include?("#{LoraRb::Settings.username}::")
    lora.quit
  end

  test 'Send a message with mqtt' do
    eui = LoraRb::Settings.test_eui
    lora = LoraClient.new

    # As first, we fill the queue to test the auto emptying
    saturate_the_queue(lora: lora)

    # [
    #   { :name=>:queued,
    #     :topic=>"reply/fractalgarden::ssg170811161533/id/170811161533",
    #     :json=>{"reply"=>{"confirmed"=>false, "id"=>415, "payload"=>"0100", "port"=>40, "priority"=>0}, "status"=>200, "eui"=>nil, "port"=>nil}},
    #   { :name=>:transmitted,
    #     :topic=>"/sub/v1/users/fractalgarden/apps/9955/devices/be7a00000000123c/events/downlink",
    #     :json=>{"reply"=>{"confirmed"=>false, "id"=>416, "payload"=>"0200", "port"=>40, "priority"=>0}, "status"=>200, "eui"=>nil, "port"=>nil}}
    # ]

    # This should fail ...but we don't wait the response
    response = lora.send_cmd(eui: eui, data: '0100', port: 41, confirmed: false, wait_response: false, delete_previous_enqueued_commands: false)
    assert response.empty?

    # Check lib/config/config.rb:19 config.downlink_response_urls
    ar_response_check = [:queued]

    # This should fail
    responses = lora.send_cmd(eui: eui, data: '0100', port: 41, confirmed: false, wait_response: true, delete_previous_enqueued_commands: false)
    assert responses.is_a? Array
    assert responses.size == ar_response_check.size, "It was expected to receive #{ar_response_check.size} messages, not #{responses.size}. Inspect:#{responses}"
    responses.each_with_index do |response, i|
      if response[:name] == :queued
        assert response[:json]['status'] == 400, "the status should be 400, not #{response[:json]['status']}, response: #{response}"
        assert response[:json]['error'] == 12, "the error should be 12, not #{response[:json]['error']}, response: #{response}"
        assert_nil response[:json]['reply']
      end
    end

    # This should empty the queue and send the cmd, by default delete_previous_enqueued_commands is true
    responses = lora.send_cmd(eui: eui, data: '0100', port: 41, wait_response: true)
    responses.each_with_index do |response, i|
      # assert ar_response_check.include?(response[:name]), "The key #{response[:name]} should be in the response #{response}"
      if response[:name] == :queued
        assert response[:json]['status'] == 200, "the status should be 200, not #{response[:json]['status']} response: #{response}"
        assert response[:json]['reply'].is_a? Hash
      end
      if response[:name] == :transmitted
        assert response['eui'] == eui, "response should have the eui #{eui}, not #{response['eui']}. Check the response #{response}"
      end
    end
    lora.quit
  end

  test 'Receive a message with mqtt' do
    eui = LoraRb::Settings.test_eui
    lora = LoraClient.new
    puts '### PRESS THE RELI BUTTON TO LISTEN ###'
    # Send an uplink message to complete the test
    lora.listen(test: true) do |topic, response|
      puts 'Pressed the button'
      assert_not_nil topic
      # assert topic.include?(eui), "topic #{topic} should include eui #{eui}"
      assert_not_nil response
      assert response.is_a?(Hash), "response #{response} should be an Hash, not #{response.class.name}"
      # assert response['eui'] == eui, "response #{response} should contains eui"
      assert_not_nil response['eui'], "response #{response} should contains eui"
      assert_not_nil response['port'], "response #{response} should contains port"
    end

    puts "### Pause 30 SECS to wait for a possible duty cycle! ###"
    sleep 30

    puts "### PRESS THE RELI BUTTON AGAIN WITHIN #{LoraRb.configuration.timeout} SECS! ###"
    topic, response = lora.read_data
    puts 'Pressed the button'
    assert_not_nil topic
    assert response.is_a?(Hash), "response #{response} should be an Hash, not #{response.class.name}"
    assert_not_nil response['eui'], "response #{response} should contains eui"
    assert_not_nil response['port'], "response #{response} should contains port"
    lora.quit
  end

  # This test fill the queue
  test 'Get enqueued messages with mqtt' do
    eui = LoraRb::Settings.test_eui
    lora = LoraClient.new

    saturate_the_queue(lora: lora)

    # ["reply/fractalgarden::ssg170928154037/id/170928154041",
    #   {"reply"=>[
    #       {"confirmed"=>true, "id"=>5034, "payload"=>"0200", "port"=>40, "priority"=>0},
    #       {"confirmed"=>true, "id"=>5035, "payload"=>"0200", "port"=>40, "priority"=>0},
    #       {"confirmed"=>true, "id"=>5036, "payload"=>"0200", "port"=>40, "priority"=>0},
    #       {"confirmed"=>true, "id"=>5037, "payload"=>"0200", "port"=>40, "priority"=>0}
    #     ],
    #     "status"=>200}]
    response = lora.get_enqueued_messages(eui: eui, debug: true)
    assert response.is_a? Array
    assert response.size == 2, "It was expected to receive an array with topic+reply, not #{response.size}"
    assert response.last.is_a?(Hash), "It was expected to receive an hash as 'reply'. Inspect: #{response}"
    assert response.last['reply'].is_a?(Array), "It was expected to receive an array in 'reply'. Inspect: #{response}"
    assert response.last['status'] == 200, "It was expected to receive a status 200. Inspect: #{response}"
    assert response.last['reply'].first.is_a?(Hash), "It was expected to receive an hash as element in reply. Inspect: #{response}"
    assert response.last['reply'].first['id'].is_a?(Fixnum), "It was expected to receive a Fixnum as id. Inspect: #{response}"

    lora.quit
  end

  # Must be after the queue has been filled
  test 'Delete enqueued messages with mqtt' do
    eui = LoraRb::Settings.test_eui
    lora = LoraClient.new

    saturate_the_queue(lora: lora)

    # [
    #   "reply/fractalgarden::ssg1709281818091d/id/17092818180996",
    #   [
    #     {"reply"=>{"confirmed"=>false, "id"=>5159, "payload"=>"0000", "port"=>40, "priority"=>0}, "status"=>200},
    #     {"reply"=>{"confirmed"=>false, "id"=>5160, "payload"=>"0000", "port"=>40, "priority"=>0}, "status"=>200},
    #     {"reply"=>{"confirmed"=>false, "id"=>5161, "payload"=>"0000", "port"=>40, "priority"=>0}, "status"=>200},
    #     {"reply"=>{"confirmed"=>false, "id"=>5162, "payload"=>"0000", "port"=>40, "priority"=>0}, "status"=>200}
    #   ]
    # ]
    response = lora.delete_enqueued_messages(eui: eui, debug: true)
    assert response.is_a? Array
    assert response.size == 2, "It was expected to receive an array with topic+[{reply=>...}], not #{response.size}"
    assert response.last.is_a?(Array), "It was expected to receive an array of hash 'reply'. Inspect: #{response}"
    assert response.last.first.is_a?(Hash), "It was expected to receive an hash in the response. Inspect: #{response}"
    assert response.last.first['reply'].is_a?(Hash), "It was expected to receive an hash as 'reply'. Inspect: #{response}"
    assert response.last.first['reply']['id'].is_a?(Fixnum), "It was expected to receive a Fixnum as id. Inspect: #{response}"

    # Now the queue must be empty
    response = lora.delete_enqueued_messages(eui: eui, debug: true)
    assert response.is_a? Array
    assert response.size == 2, "It was expected to receive an array with topic+[{reply=>...}], not #{response.size}"
    assert response.last.empty?, "It was expected to receive an empty array instead a 'reply'. Inspect: #{response}"

    lora.quit
  end


# Fill the queue up to saturation
  def saturate_the_queue(lora:, eui:LoraRb::Settings.test_eui)
    4.times do
      lora.send_cmd(eui: eui,data:'0000',confirmed: false,wait_response: false,delete_previous_enqueued_commands: false)
    end
  end
end

