module LoraRb
  class Configuration
    # attr_writer :allow_sign_up

    attr_accessor :provider, :protocol, :host, :port, :uplink_url, :downlink_url, :downlink_response_urls,
                  :ssl, :ssl_file, :env, :timeout, :enqueued_downlinks_url, :delete_enqueued_downlink_url

    def initialize
      @provider = nil #PROVIDERS.keys.first
      @protocol = nil #PROVIDERS[@provider].first
      @host = nil
      @port = nil
      @uplink_url = nil
      @downlink_url = nil
      @enqueued_downlinks_url = nil
      @downlink_response_urls = nil
      @delete_enqueued_downlink_url = nil
      @ssl = nil # True if uses tls
      @ssl_file = nil # The certificate's path
      @env = 'development'
      @timeout = 10.0
    end
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configuration=(config)
    @configuration = config
  end

  def self.configure
    yield configuration
  end
end