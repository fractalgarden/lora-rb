# LoraRb methods
module LoraRb
  # It contains protocols methods
  class Protocol

    PROVIDERS = { fractalgarden: [:mqtt],
                  a2a: [:mqtt],
                  loriot: [:tls],
                  resiot: [:http]
    }

    def self.provider_is_valid?(provider: LoraRb.configuration.provider)
      PROVIDERS.has_key? provider
    end
    def self.supported?(provider: LoraRb.configuration.provider,
                                 protocol: LoraRb.configuration.protocol)
      return unless provider && protocol
      PROVIDERS[provider] && PROVIDERS[provider].include?(protocol.to_sym)
    end
  end
end