require 'rails/generators/base'

module LoraRb
  module Generators
    class InstallGenerator < Rails::Generators::Base
      desc 'Installs LoraRb configuration files'
      source_root File.expand_path('../../', __FILE__)
      # class_option :protocol, :type => :string, :default => 'tls', :desc => "Choose your favorite emulator [tls]"

      def copy_to_local
        # copy_file '../config/settings.yml', 'config/lora_rb.yml'
        copy_file '../config/private_EXAMPLE.yml', 'config/lora_rb_private.yml'
        file = 'config/initializers/lora-rb.rb'
        copy_file '../config/config.rb', file
        append_file file do
          <<-FILE.gsub(/^            /, '')
            connection_protocol = LoraRb.configuration.protocol
            raise 'Define your protocol in the configuration file!' unless connection_protocol
            raise "Connection protocol #{connection_protocol} not recognized!" unless LoraRb::Protocol.supported_protocols.include?(connection_protocol.to_s)
            require "lora-rb/#{connection_protocol}/call"
            LoraClient.include LoraRb::Call
          
            %w(lora_rb_private.yml).each  do |file|
              filepath = File.join(Rails.root,'config',file)
              LoraRb::Settings.load!(filepath,env: LoraRb.configuration.env) if File.exist? filepath
            end
            raise 'Insert your secret data to login on the lora cloud!' unless LoraRb::Settings.appid.present? && LoraRb::Settings.token.present?
          FILE
        end
      end
    end
  end
end