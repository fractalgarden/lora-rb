LoraRb.configure do |config|
  # These informations are shared for all environments
  # config.protocol = :tls
  # config.host = 'eu1.loriot.io'
  # config.port = 737
  # config.protocol = :rabbitmq
  # config.host = 'localhost'
  # config.protocol = :http
  # config.host = 'eu72.resiot.io'
  # A2a
  config.provider = :a2a
  config.protocol = :mqtt
  config.host = 'ptnetsuite.a2asmartcity.io'
  config.port = 8883
  config.uplink_url = '/sub/v1/users/{username}/apps/{appid}/devices/+/uplink/+'
  config.downlink_url = '/api/v1/users/{username}/apps/{appid}/devices/{deveui}/downlink/post/reply/{clientid}/id/{requestid}'
  config.delete_enqueued_downlink_url = '/api/v1/users/{username}/apps/{appid}/devices/{deveui}/downlink/{id}/delete/reply/{clientid}/id/{requestid}'
  config.enqueued_downlinks_url = '/api/v1/users/{username}/apps/{appid}/devices/{deveui}/downlink/get/reply/{clientid}/id/{requestid}'
  config.timeout = 60.0
  # If you expect more than one response in a topic, add an item with url=nil to not subscribe it newly
  config.downlink_response_urls = [
      { name: :queued, url: 'reply/{clientid}/id/{requestid}'},
      # { name: :transmitted, url: '/sub/v1/users/{username}/apps/{appid}/devices/{deveui}/events/downlink' }
      # { name: :confirmed, url: nil },
  ]
  config.ssl = true
  config.ssl_file = 'ssl_certificates/mqtt_ca.crt'
  # Fractalgarden Free Network Server
  # config.provider = :fractalgarden
  # config.protocol = :mqtt
  # config.host = '192.168.1.106'
  # config.port = 1883
  # config.uplink_url = 'application/{appid}/node/+/rx'
  # config.downlink_url = 'application/{appid}/node/{deveui}/tx'
  # config.timeout = 30.0
  # # If you expect more than one response in a topic, ad a item with url=nil to not subscribe it newly
  # config.downlink_response_urls = [
  #     { name: :queued, url: 'reply/{clientid}/id/{requestid}'},
  # ]
  # config.ssl = false
  # # config.ssl_file = "#{Rails.root}/vendor/ssl_certificates/a2a_mqtt_ca.crt"
end