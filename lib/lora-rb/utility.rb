# LoraRb methods
module LoraRb
  # It contains generic methods
  module Base

    # Show tips on usage
    def merge_tags_to_url(url, tags={}, options={})
      url = url.dup
      puts "merge_tags_to_url: #{url}" if options[:debug] == :full
      tags.each do |tag, value|
        puts " replace {#{tag}} with #{value}" if options[:debug] == :full
        url.gsub!("{#{tag}}", value.to_s)
      end
      puts "merge_tags_to_url: completed #{url}" if options[:debug] == :full
      url
    end
  end

  private

  def puts(message)
    Kernel.puts "[#{self.class.name}##{__method__}] #{Time.now} #{message}"
  end
end