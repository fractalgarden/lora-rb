# LoraRb calling methods
module LoraRb
  # It contains all the methods for selecting the items
  module Call

    attr_reader :contex, :tcp_socket, :ssl_socket

    private

    def sub_initialize(options={})
      @context = OpenSSL::SSL::SSLContext.new
      @tcp_socket = TCPSocket.new(options[:host], options[:port])
      @ssl_socket = OpenSSL::SSL::SSLSocket.new(@tcp_socket, @context)
      @ssl_socket.sync_close = true
      @ssl_socket.connect
      welcome_response = ''
      @ssl_socket.sysread(512, welcome_response)
      puts "Welcome response: #{welcome_response}" if options[:debug]
      welcome_response
    end

    # Send the request to device
    def sub_send_cmd(options={})
      response = ''
      str_app_token = "{\"appid\":\"#{options[:appid]}\", \"token\":\"#{options[:token]}\"}"
      puts "#{Time.now} To: #{str_app_token}" if options[:debug]
      @ssl_socket.puts str_app_token
      str_request = "{\"cmd\":\"#{options[:cmd]}\",\"EUI\":\"#{options[:eui]}\",\"port\":#{options[:port]},\"confirmed\":#{options[:confirmed]},\"data\":\"#{options[:data]}\"} 	"
      puts "#{Time.now} Cmq request: #{str_request}" if options[:debug]
      @ssl_socket.puts(str_request)
      begin
        retries ||= 0
        @ssl_socket.sysread(512, response)
      rescue EOFError => e
        if (retries += 1) < 3
          puts "Attempt ##{retries}..." if options[:debug]
          sleep 0.5
          retry
        else
          raise e
        end
      end

      # Return the json hash from the string
      JSON.parse(response)
    end

    # Send the request to device
    def sub_read_data(options={})
      response = ''
      begin
        retries ||= 0
        @ssl_socket.sysread(512, response)
      rescue EOFError => e
        if (retries += 1) < 3
          puts "Attempt ##{retries}..." if options[:debug]
          sleep 0.5
          retry
        else
          raise e
        end
      end

      # Return the json hash from the string
      JSON.parse(response)
    end

    # Waiting for message
    def sub_listen(options={}, &block)
      response = nil
      begin
        response = sub_read_data(options)
        puts " [x] Received #{response}" if options[:debug]
        block.call(response) if block
      end while response && !options[:test]
    end

    # Pending ...
    def sub_get_enqueued_messages(eui:, request_id: nil, debug: false)
      raise "Pending..."
    end

    # Pending ...
    def sub_delete_enqueued_messages(eui:, request_id: nil, debug: false)
      raise "Pending..."
    end

    # Close the connection
    def sub_quit
      @ssl_socket.close
    end

  end
end