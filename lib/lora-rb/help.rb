# LoraRb methods
module LoraRb
  # It contains generic methods
  module Base
    private

    # Show tips on usage
    def lorarb_help
      <<-HELP.gsub(/^      /, '')
      *************************************************************************
      LoraRb version #{LoraRb.version}
      *************************************************************************
      HELP
    end
  end
end