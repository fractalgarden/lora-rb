require_relative 'help'
require_relative 'utility'

###################################
# An interface to Lora Service from Ruby
###################################
class LoraClient
  include LoraRb::Base

  # Establish a secure connection to your account on the cloud
  def initialize(options = {})
    options = {debug: false,
               token: LoraRb::Settings.token,
               appid: LoraRb::Settings.appid,
               host: LoraRb.configuration.host,
               port: LoraRb.configuration.port,
               uplink_url: LoraRb.configuration.uplink_url,
               downlink_url: LoraRb.configuration.downlink_url,
               enqueued_downlinks_url: LoraRb.configuration.enqueued_downlinks_url,
               downlink_response_urls: LoraRb.configuration.downlink_response_urls,
               delete_enqueued_downlink_url: LoraRb.configuration.delete_enqueued_downlink_url,
               username: LoraRb::Settings.username,
               password: LoraRb::Settings.password,
               ssl: LoraRb.configuration.ssl,
               ssl_file: LoraRb.configuration.ssl_file,
               timeout: LoraRb.configuration.timeout }.merge(options)
    @debug = options[:debug]
    @token = options[:token]
    @appid = options[:appid]

    require 'philter'
    require 'securerandom'

    raise "Specify your provider in the configuration file!" unless LoraRb.configuration.provider
    raise "Specify the protocol in the configuration file!" unless LoraRb.configuration.protocol
    raise "Provider #{LoraRb.configuration.provider} not defined!" unless LoraRb::Protocol.provider_is_valid?
    raise "Connection protocol #{LoraRb.configuration.protocol} not supported by the provider #{LoraRb.configuration.provider}!" unless LoraRb::Protocol.supported?
    require_relative "#{LoraRb.configuration.provider}/#{LoraRb.configuration.protocol}/call"
    self.extend LoraRb::Call

    welcome_response = sub_initialize(options)

    raise("Lora-rb: Cannot connect to host #{options[:host]}:#{options[:port]}") unless welcome_response.key?('hello')

  end

  # Send the request to device
  def send_cmd(options = {})
    options = { debug: @debug,
                token: @token,
                appid: @appid,
                cmd: "tx",
                eui: nil,
                port: 40,
                confirmed: false,
                data: "0301"}.merge(options)

    raise("Eui is blank! Should i guess your device?") unless options[:eui]
    response = sub_send_cmd(options)
    puts "#{Time.now} Cmd response: #{response}" if options[:debug]
    response
  end

  # Receive all data devices from the cloud
  # Each device sends its data to the cloud
  def read_data(options = {})
    options = { debug: @debug }.merge(options)
    puts "#{Time.now} Waiting for #{@appid} incoming data..." if options[:debug]
    response = sub_read_data(options)
    puts "#{Time.now} Received: #{response}" if options[:debug]
    response
  end

  # Stay awaiting data from the cloud
  def listen(options = {}, &block)
    options = { debug: @debug }.merge(options)

    puts "#{Time.now} Starting Listen app #{@appid}. To exit press CTRL+C" if options[:debug]
    sub_listen(options, &block)
  end

  # It returns enqueued message ready to delivery to node
  # ['topic', {'reply' => [ { ... } ], 'status' => 200}]
  def get_enqueued_messages(eui:, request_id: nil, debug: false)
    res = sub_get_enqueued_messages(eui: eui,
                                    request_id: request_id,
                                    debug: debug)
    puts "get_enqueued_message #{res}" if @debug
    res
  end

  # It returns enqueued message ready to delivery to node
  # ['topic', [ {'reply' => { ... }, 'status' => 200 ] ]
  def delete_enqueued_messages(eui:, id: nil, debug: false)
    res = sub_delete_enqueued_messages(eui: eui,
                                       id: id,
                                       debug: debug)
    puts "delete_enqueued_message #{res}" if @debug
    res
  end
  # Close the secure connection with the cloud
  def quit
    sub_quit
  end
end