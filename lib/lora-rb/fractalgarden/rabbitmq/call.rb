# LoraRb calling methods
module LoraRb
  # It contains all the methods for selecting the items
  module Call

    attr_reader :client

    private

    def sub_initialize(options={})
      require 'bunny'
      @connection = Bunny.new(hostname: options[:host])
      @connection.start
      @channel = @connection.create_channel
      @queue = @channel.queue(options[:queue])
      {'hello' => 'Lora-Rb: Ready to start!'}
    end

    # Send the request to device
    def sub_send_cmd(options={})
      str_request = "{\"cmd\":\"#{options[:cmd]}\",\"EUI\":\"#{options[:eui]}\",\"port\":#{options[:port]},\"confirmed\":#{options[:confirmed]},\"data\":\"#{options[:data]}\"} 	"
      puts "#{Time.now} Cmq request: #{str_request}" if options[:debug]
      response = @channel.default_exchange.publish(str_request, routing_key: @queue.name)
      puts " [x] Sent '#{str_request}'" if options[:debug]
      response
    end

    # Receive the payload from the network
    def sub_read_data(options={})
      delivery_info, metadata, payload = @queue.pop
      puts " [x] Received #{delivery_info} #{metadata} #{payload}" if options[:debug]
      return delivery_info, metadata, payload
    end

    # Waiting for message in the queue
    def sub_listen(options={}, &block)
      puts " [*] Waiting for messages in #{@queue.name}. To exit press CTRL+C" if options[:debug]
      @queue.subscribe(block: true) do |delivery_info, properties, body|
        puts " [x] Received #{body}" if options[:debug]
        block.call(delivery_info, properties, body) if block
        # cancel the consumer to exit
        delivery_info.consumer.cancel
      end
    end

    # Pending ...
    def sub_get_enqueued_messages(eui:, request_id: nil, debug: false)
      raise "Pending..."
    end

    # Pending ...
    def sub_delete_enqueued_messages(eui:, request_id: nil, debug: false)
      raise "Pending..."
    end

    # Close the connection
    def sub_quit
      @connection.close
    end

  end
end

# Monkey patch since Bunny use the rails method .empty?
class NilClass
  def empty?; true end
end