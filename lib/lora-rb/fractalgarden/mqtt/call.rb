# LoraRb calling methods
module LoraRb
  class EuiNotFound < StandardError; end
  # It contains all the methods for selecting the items
  module Call
    attr_reader :client

    private

    def sub_initialize(options={})
      require 'mqtt'

      raise 'Specify uplink_url to continue!'             unless options[:uplink_url]
      raise 'Specify downlink_url to continue!'           unless options[:downlink_url]
      raise 'Specify downlink_response_urls to continue!' unless options[:downlink_response_urls]
      raise 'Specify host to continue!'                   unless options[:host]
      raise 'Specify port to continue!'                   unless options[:port]
      raise 'Specify username to continue!'               unless options[:username]
      raise 'Specify password to continue!'               unless options[:password]
      raise 'Specify ssl_file to continue!'               if options[:ssl] && !options[:ssl_file]

      @username = options[:username]
      @client_id = "#{@username}::ssg#{generate_request_id}"

      connection_attributes = {
          host: options[:host],
          port: options[:port],
          ssl:  options[:ssl],
          username: @username,
          password: options[:password],
          client_id: @client_id
      }
      connection_attributes[:cert_file] = options[:ssl_file] if connection_attributes[:ssl]
      # The class variable @@mock is set by the stubbed class. The other methods (send_cmd etc.) are entirely stubbed.
      @client = MQTT::Client.connect(connection_attributes) unless defined?(@@mock) && @@mock

      @topic = merge_tags_to_url(options[:uplink_url],
                                 { username: @username,
                                   appid: @appid },
                                 { debug: options[:debug] })

      @downlink_url = options[:downlink_url]
      @downlink_response_urls = options[:downlink_response_urls]
      @enqueued_downlinks_url = options[:enqueued_downlinks_url]
      @delete_enqueued_downlink_url = options[:delete_enqueued_downlink_url]
      @timeout = options[:timeout]
      @wait_response = options.has_key?(:wait_response) ? options[:wait_response] : true
      {'hello' => 'Lora-Rb: Ready to start!'}
    end

    # Send the request to device
    def sub_send_cmd(options={})
      options = { wait_response: @wait_response }.merge(options)

      request_id = generate_request_id
      h_request = {
          "fport": options[:port],              # Port where the message should be sent
          "data": options[:data],               # Message payload
          "confirmed": options[:confirmed],     # (Optional) Tells whether a confirmed downlink is requested. Default: false
          "devEUI": options[:eui],
          "reference": request_id
      }

      publish_url = merge_tags_to_url(@downlink_url,
                                       username: @username,
                                       appid: @appid,
                                       deveui: options[:eui],
                                       clientid: @client_id,
                                       requestid: request_id)

      puts "  publish #{h_request.to_json} to #{publish_url}" if options[:debug]
      responses = []
      response_topics = []
      # thr_response = nil
      if options[:wait_response]
        @downlink_response_urls.each do |dru_hash|
          name, response_url = dru_hash[:name], dru_hash[:url]
          next unless response_url

          # response_topic = "reply/#{@client_id}/id/#{request_id}"
          response_topic = merge_tags_to_url(response_url,
                                             username: @username,
                                             appid: @appid,
                                             deveui: options[:eui],
                                             clientid: @client_id,
                                             requestid: request_id)
          puts "  Subscribing response #{response_topic}" if options[:debug] == :full
          response_topics << response_topic
        end

        raise "cannot subscribe without topic, response_topics is empty!" if response_topics.empty?
        @client.subscribe(*response_topics)
      end

      @client.publish(publish_url, h_request.to_json, false)

      if options[:wait_response]
        # Waiting for all the responses
        # thr_responses.each { |thr_response| thr_response.join }
        # thr_response.join
        @downlink_response_urls.each do |dru_hash|
          response_topic, response_message = sub_read_data(topic: nil, debug: options[:debug])
          response = { topic: response_topic, json: response_message }
          puts "  Found response #{response} " if options[:debug] == :full
          responses << response
        end

        @client.unsubscribe(*response_topics)
      end
      responses
    end

    # Receive the payload from the network
    # There is a timeout. Use this method only to retrieve data from the queue. If you have to waiting data
    # please use listen.
    # If topic is nil it uses subscribed topics
    def sub_read_data(options={})
      topic = options.has_key?(:topic) ? options[:topic] : @topic
      if topic
        topic = topic.dup
        puts "  Reading topic #{topic}..." if options[:debug]
      end

      message = nil
      begin
        Timeout.timeout(options[:timeout] || @timeout) do
          topic, message = @client.get(topic)
        end
      rescue Timeout::Error
        message = {error: 'Timeout'}.to_json
      end

      message = JSON.parse(message)
      if message.respond_to? '[]'
        eui = get_eui_from_topic(topic)
        message['eui'] ||= eui if eui
        port = get_port_from_topic(topic)
        message['port'] ||= port if port
      end
      return topic, message
    end

    # Waiting for message in the queue
    def sub_listen(options={}, &block)
      topic = options.has_key?(:topic) ? options[:topic] : @topic
      if topic
        topic = topic.dup
        puts "  Waiting for messages in #{topic}. To exit press CTRL+C" if options[:debug]
      end

      @client.get(topic) do |topic, message|
        # Block is executed for every message received
        puts "  original json: #{topic}: #{message}" if options[:debug] == :full
        message = JSON.parse(message)
        if message.respond_to? '[]'
          eui = get_eui_from_topic(topic)
          message['eui'] ||= eui if eui
          port = get_port_from_topic(topic)
          message['port'] ||= port if port
        end
        puts "  #{message}" if options[:debug] #After reworking
        block.call(topic, message) if block_given?
        break if options[:test]
      end
    end

    # Close the connection
    def sub_quit
      @client.disconnect
    end

    def get_eui_from_topic(topic)
      res = topic&.match(/devices\/(\w{16})/)
      res[1] if res
    end

    def get_port_from_topic(topic)
      res = topic&.match(/uplink\/(\d+)(\/|\Z)/)
      res[1] if res
    end

    def generate_request_id
      "#{Time.now.strftime('%y%m%d%H%M%S')}#{SecureRandom.hex(1)}"
    end

  end
end
