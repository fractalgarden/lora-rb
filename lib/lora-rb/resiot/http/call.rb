# LoraRb calling methods
module LoraRb
  # It contains all the methods for selecting the items
  module Call

    private

    def sub_initialize(options={})
      require 'http'
      @host = options[:host]
      {'hello' => 'Lora-Rb: Ready to start!'}
    end

    # Downlink: Send the request to the device
    def sub_send_cmd(options={})
      json_request = {
              "Command": "",
              "appEUI": options[:appid],
              "askConfirmation": options[:confirmed],
              "devEUI": options[:eui],
              "payload": options[:data],
              "hexIdConnector": LoraRb::Settings.connector_id,
              "port": options[:port],
              "raw": false
      }

      puts "#{Time.now} Cmq request: #{json_request}" if options[:debug]
      response = HTTP.headers(_http_headers).post("https://#{@host}/api/application/#{@appid}/nodes/#{options[:eui]}/downlink", json: json_request)
      return response if options[:debug] == :full
      # Return the json hash from the string
      ar = response.to_a
      # Example:
      # [500,
      #  {"Content-Type"=>"application/json", "Date"=>"Thu, 29 Jun 2017 16:32:37 GMT", "Content-Length"=>"109", "Connection"=>"close"},
      #  "{\"error\":\"Node [AppEui: 1234, DevEui: abcd000000001234] not found in your collection.\",\"code\":2}"
      # ]
      {'status_code' => ar[0], 'request' => json_request}.merge(ar[1]).merge(JSON.parse(ar[2]))
    end

    # Uplink: http waits data with an http server.
    # Lora Network server must exec an http push
    def sub_read_data(options={})
      raise('In http protocol, it expects data with an http server. Lora Network server must exec an http push ')
    end

    # Waiting for messages
    def sub_listen(options={})
      raise('In http protocol, it expects data with an http server. Lora Network server must exec an http push ')
    end

    # Pending ...
    def sub_get_enqueued_messages(eui:, request_id: nil, debug: false)
      raise "Pending..."
    end

    # Pending ...
    def sub_delete_enqueued_messages(eui:, request_id: nil, debug: false)
      raise "Pending..."
    end

    # Blank method. No need to close a connection with the http protocol
    def sub_quit
    end

    def _http_headers(headers={})
      headers.merge('Content-Type' => 'application/json', 'Authorization' => @token)
    end

  end
end