# LoraRb
# Copyright (C) 2017  Marco Mastrodonato, marco.mastrodonato@fractalgarden.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'core/base'

module LoraRb
  def self.root
    File.expand_path('../..', __FILE__)
  end
end

require_relative 'lora-rb/base'

unless defined?(Rails)
  require_relative 'config/config'

  %w(private.yml).each  do |file|
    filepath = File.join(LoraRb.root, 'lib', 'config', file)
    LoraRb::Settings.load!(filepath, env: LoraRb.configuration.env) if File.exist? filepath
  end
  raise "Insert your secret data to login on the lora network server!" if LoraRb::Settings.appid.nil? || LoraRb::Settings.token.nil?
end

