v0.15.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.14.0..v0.15.0) November 1th, 2017
------------------------------
* Added new parameter "provider" to connection's configuration 
* Added provider fractalgarden as new provider which support mqtt protocol. It offers free service, try it!
* LoraClient class is now extended by the provider's methods on the initialization. In the future, this will make it easier 
  to connect to multiple network servers.
* Several improvements

v0.14.1 October 20th, 2017
------------------------------
* Mqtt: method all_enqueued_downlink_id raise the custom exception EuiNotFound 
        if eui does not exists

v0.14.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.13.0..v0.14.0) September 28th, 2017
------------------------------
* Mqtt: 
    * Read data and listen does not add eui and port anymore if they are nil
    * Added new method get_enqueued_messages to get all the enqueued downlinks that will be sent to a eui
    * Added new method delete_enqueued_messages to delete one or all the enqueued downlinks for a given a eui
    * Now send cmd clean the device's enqueued messages before sending a new one
    * Improved unit tests

v0.13.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.12.0..v0.13.0) September 12th, 2017
------------------------------
* Mqtt: in the send_cmd, qos parameter is not set by default anymore

v0.12.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.11.0..v0.12.0) August 28th, 2017
------------------------------
* Mqtt:
    * send_cmd doesn't use thread anymore. Now it subscribes each topics then read data, 
      ones for each downlink_response_urls
    * added timeout to the configuration params
    * read_data now has a timeout after which it raises a Timeout::Error. Listen, on the other hand, doesn't have it. 
    * read_data and listen now can use subscribed topics. To do that, set topic argument to nil
    * downlink_response_urls now is an array, before it was an hash

v0.11.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.10.0..v0.11.0) August 10th, 2017
------------------------------
* Mqtt:
    * now you can manage response from the configuration and define multiple response topics: queued, transmitted...
    * send cmd message now contains multiple informations

v0.10.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.9.0..v0.10.0) August 8th, 2017
------------------------------
* Mqtt: 
    * added multi threading support
    * send_cmd now set wait_response: true by default. With this param, the response is waited using a separate thread

v0.9.3 August 8th, 2017
------------------------------
* Fixed a bug with mqtt protocol: now when LoraClient is instantiated uses the new client id just generated
* Mqtt: Send cmd now use the new parameter wait_response to wait the response on the topic response, not confirmed param anymore.

v0.9.2 August 7th, 2017
------------------------------
* Fixed a bug: listen now use the block

v0.9.1 August 7th, 2017
------------------------------
* Added port to message uplink via mqtt, EUI is now eui

v0.9.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.8.0..v0.9.0) August 7th, 2017
------------------------------
* Mqtt protocol renamed to Rabbitmq and new generic mqtt protocol added
* Mqtt protocol now support tls
* Now support the italian lora network A2a Smart City
* Configuration params updated to meet the mqtt requirements
* Updated readme

v0.8.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.7.0..v0.8.0) July 11th, 2017
------------------------------
* Send_cmd now accept param confirmed true/false
* New param connector_id
* Http response now contains the request as well

v0.7.2 June 30th, 2017
------------------------------
* Configuration yaml now use Erb if available

v0.7.1 June 30th, 2017
------------------------------
* Fixed send cmd for http protocol: It now returns a hash, previously a string

v0.7.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.6.0..v0.7.0) June 29th, 2017
------------------------------
* Http send cmd now return an hash with all the network data

v0.6.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.5.0..v0.6.0) June 28th, 2017
------------------------------
* Now support mqtt to get and receive data throught RabbitMQ
* Code cleaning

v0.5.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.4.0..v0.5.0) June 27th, 2017
------------------------------
* Now support http push to send data with an internal http client (only send data, to receive you must have an http server)
* Now support the italian lora network Resiot.io

v0.4.0 [☰](https://bitbucket.org/fractalgarden/loriot-rb/branches/compare/v0.3.0..v0.4.0) April 7th, 2017
------------------------------
* the method `listen` now accept a block to do something every iteration, moreover, it has been included in the unit test.

v0.3.0 April 7th, 2017
------------------------------
* added the configuration in the config.rb 
* moved sensible data to a yaml accessible dinamically with LoriotRb::Settings 
* added a rails generator
* improved the code organization
* improved the readme

v0.2.0 April 5th, 2017
------------------------------
* dividing the project into modules based on the connection protocol
* new method listen to waiting for data
* improved debug option

v0.1.0 March 10th, 2017
------------------------------
* Starting project
* Connection with tls
* Basic unit test